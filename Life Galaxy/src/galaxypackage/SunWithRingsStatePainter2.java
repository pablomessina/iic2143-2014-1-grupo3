/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author Pablo
 */
public class SunWithRingsStatePainter2 extends StatePainter {

    @Override
    public void paint(Graphics2D g2d, SolarSystem entity) {
        paintCircle2(g2d, entity.centerX, entity.centerY, entity.sunRadius, entity.sunColor);
        for (Ring ring : entity.rings) {
            paintRing(g2d, ring);
        }
    }

    private void paintRing(Graphics2D g2d, Ring ring) {
        double centerX = ring.owner.centerX;
        double centerY = ring.owner.centerY;
        double outerRadius = ring.outerRadius;
        if (isThereIntersection(centerX - outerRadius, centerX + outerRadius, centerY - outerRadius, centerY + outerRadius,
                GalaxyFrame.bottomLeftX, GalaxyFrame.bottomLeftX + GalaxyCanvas.gridWidth,
                GalaxyFrame.bottomLeftY, GalaxyFrame.bottomLeftY + GalaxyCanvas.gridHeight));
        {
            double innerRadius = ring.innerRadius;
            Ellipse2D.Double outer =
                    new Ellipse2D.Double((centerX - GalaxyFrame.bottomLeftX - outerRadius) * GalaxyCanvas.cellWidth,
                    (GalaxyCanvas.gridHeight - centerY + GalaxyFrame.bottomLeftY - outerRadius) * GalaxyCanvas.cellHeight,
                    2 * outerRadius * GalaxyCanvas.cellWidth,
                    2 * outerRadius * GalaxyCanvas.cellHeight);
            Ellipse2D.Double inner =
                    new Ellipse2D.Double((centerX - GalaxyFrame.bottomLeftX - innerRadius) * GalaxyCanvas.cellWidth,
                    (GalaxyCanvas.gridHeight - centerY + GalaxyFrame.bottomLeftY - innerRadius) * GalaxyCanvas.cellHeight,
                    2 * innerRadius * GalaxyCanvas.cellWidth,
                    2 * innerRadius * GalaxyCanvas.cellHeight);
            Area ringArea = new Area(outer);
            ringArea.subtract(new Area(inner));
            g2d.setColor(ring.color);
            g2d.fill(ringArea);
        }
    }
}
