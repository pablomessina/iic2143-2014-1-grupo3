/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

import java.awt.Graphics2D;

/**
 *
 * @author Pablo
 */
public class SunWithRingsStatePainter1 extends StatePainter {

    @Override
    public void paint(Graphics2D g2d, SolarSystem entity) {
        paintCircle1(g2d, entity.centerX, entity.centerY, entity.sunRadius, entity.sunColor);
        for (Ring ring : entity.rings) {
            paintRing(g2d, ring);
        }
    }

    private void paintRing(Graphics2D g2d, Ring ring) {
        double centerX = ring.owner.centerX;
        double centerY = ring.owner.centerY;
        double outerRadius = ring.outerRadius;;
        if (isThereIntersection(centerX - outerRadius, centerX + outerRadius, centerY - outerRadius, centerY + outerRadius,
                GalaxyFrame.bottomLeftX, GalaxyFrame.bottomLeftX + GalaxyCanvas.gridWidth,
                GalaxyFrame.bottomLeftY, GalaxyFrame.bottomLeftY + GalaxyCanvas.gridHeight));
        {
            int i, j;
            int xstart = (int) (centerX - outerRadius - 1);
            int xend = (int) (centerX + outerRadius + 1);
            int ystart = (int) (centerY - outerRadius - 1);
            int yend = (int) (centerY + outerRadius + 1);
            g2d.setColor(ring.color);

            for (int x = xstart; x <= xend; x++) {
                for (int y = ystart; y <= yend; y++) {
                    if (isInsideCircle(x + 0.5, y + 0.5, centerX, centerY, outerRadius)
                            && isInsideRectangle(x, y, GalaxyFrame.bottomLeftX, GalaxyFrame.bottomLeftY,
                            GalaxyCanvas.gridWidth, GalaxyCanvas.gridHeight)
                            && !isInsideCircle(x + 0.5, y + 0.5, centerX, centerY, ring.innerRadius)) {
                        i = x - GalaxyFrame.bottomLeftX;
                        j = y - GalaxyFrame.bottomLeftY;
                        g2d.fillRect((int) (i * GalaxyCanvas.cellWidth + 0.5),
                                (int) ((GalaxyCanvas.gridHeight - 1 - j) * GalaxyCanvas.cellHeight + 0.5),
                                (int) (GalaxyCanvas.cellWidth + 0.5),
                                (int) (GalaxyCanvas.cellHeight + 0.5)); 
                    }
                }
            }
        }
    }
}
