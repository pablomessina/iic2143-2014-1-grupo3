/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

import java.awt.Color;

/**
 *
 * @author Pablo
 */
public class CloudStateCollisionManager extends CollisionsManager {

    @Override
    protected void checkCollisionAgainstCloud(SolarSystem entity1, SolarSystem entity2) {
        /*if(isThereIntersection(entity1.centerX - entity1.sunRadius, entity1.centerX + entity1.sunRadius, 
         entity1.centerY - entity1.sunRadius, entity1.centerY + entity1.sunRadius,
         entity2.centerX - entity2.sunRadius, entity2.centerX + entity2.sunRadius,
         entity2.centerY - entity2.sunRadius, entity2.centerY + entity2.sunRadius)*/
        if (isThereIntersectionCircles(entity1.centerX, entity1.centerY, entity2.centerX, entity2.centerY, entity1.sunRadius, entity2.sunRadius)) {
            double mass = entity1.mass + entity2.mass;
            double r = Math.sqrt(entity1.sunRadius * entity1.sunRadius + entity2.sunRadius * entity2.sunRadius);
            double cX = (entity1.centerX * entity1.mass + entity2.centerX * entity2.mass) / mass;
            double cY = (entity1.centerY * entity1.mass + entity2.centerY * entity2.mass) / mass;
            double vX = (entity1.velX * entity1.mass + entity2.velX * entity2.mass) / mass;
            double vY = (entity1.velY * entity1.mass + entity2.velY * entity2.mass) / mass;
            Color col1 = entity1.sunColor;
            Color col2 = entity2.sunColor;
            int red = (col1.getRed() + col2.getRed()) / 2;
            int green = (col1.getGreen() + col2.getGreen()) / 2;
            int blue = (col1.getBlue() + col2.getBlue()) / 2;
            Color color = new Color(red, green, blue);
            SolarSystem newEntity = new SolarSystem(r, cX, cY, vX, vY, mass, color);
            GalaxyFrame.deleteEntity(entity1);
            GalaxyFrame.deleteEntity(entity2);
            GalaxyFrame.addEntity(newEntity);
        }
    }

    @Override
    protected void checkCollisionAgainstSunWithRings(SolarSystem entity1, SolarSystem entity2) {
        double totalmass;
        Ring ring;
        for (int i = entity2.rings.size() - 1; i >= 0; --i) {
            ring = entity2.rings.get(i);
            if (isThereIntersectionCircles(entity1.centerX, entity1.centerY, entity2.centerX, entity2.centerY, entity1.sunRadius, ring.outerRadius)) {
                totalmass = entity1.mass + ring.mass;
                Color newColor = new Color((int) ((entity1.sunColor.getRed() * entity1.mass + ring.color.getRed() * ring.mass) / totalmass),
                        (int) ((entity1.sunColor.getGreen() * entity1.mass + ring.color.getGreen() * ring.mass) / totalmass),
                        (int) ((entity1.sunColor.getBlue() * entity1.mass + ring.color.getBlue() * ring.mass) / totalmass));
                entity1.sunColor = newColor;
                entity1.mass = totalmass;
                entity2.rings.remove(i);
            } else {
                return;
            }
        }
        if (isThereIntersectionCircles(entity1.centerX, entity1.centerY, entity2.centerX, entity2.centerY, entity1.sunRadius, entity2.sunRadius)) {
            double mass = entity1.mass + entity2.mass;
            double r = Math.sqrt(entity1.sunRadius * entity1.sunRadius + entity2.sunRadius * entity2.sunRadius);
            double cX = (entity1.centerX * entity1.mass + entity2.centerX * entity2.mass) / mass;
            double cY = (entity1.centerY * entity1.mass + entity2.centerY * entity2.mass) / mass;
            double vX = (entity1.velX * entity1.mass + entity2.velX * entity2.mass) / mass;
            double vY = (entity1.velY * entity1.mass + entity2.velY * entity2.mass) / mass;
            Color col1 = entity1.sunColor;
            Color col2 = entity2.sunColor;
            int red = (col1.getRed() + col2.getRed()) / 2;
            int green = (col1.getGreen() + col2.getGreen()) / 2;
            int blue = (col1.getBlue() + col2.getBlue()) / 2;
            Color color = new Color(red, green, blue);
            SolarSystem newEntity = new SolarSystem(r, cX, cY, vX, vY, mass, color);
            GalaxyFrame.deleteEntity(entity1);
            GalaxyFrame.deleteEntity(entity2);
            GalaxyFrame.addEntity(newEntity);
        }
    }

    @Override
    protected void checkCollisionAgainstSunWithPlanets(SolarSystem entity1, SolarSystem entity2) {
        double totalmass;
        Planet planet;
        for (int i = entity2.planets.size() - 1; i >= 0; --i) {
            planet = entity2.planets.get(i);
            if (isThereIntersectionCircles(entity1.centerX, entity1.centerY, planet.centerX, planet.centerY, entity1.sunRadius, planet.radius)) {
                totalmass = entity1.mass + planet.mass;
                Color newColor = new Color((int) ((entity1.sunColor.getBlue() * entity1.mass + planet.color.getBlue() * planet.mass) / totalmass),
                        (int) ((entity1.sunColor.getBlue() * entity1.mass + planet.color.getBlue() * planet.mass) / totalmass),
                        (int) ((entity1.sunColor.getBlue() * entity1.mass + planet.color.getBlue() * planet.mass) / totalmass));
                entity1.sunColor = newColor;
                entity1.mass = totalmass;
                entity2.planets.remove(i);
            }
        }
        if (isThereIntersectionCircles(entity1.centerX, entity1.centerY, entity2.centerX, entity2.centerY, entity1.sunRadius, entity2.sunRadius)) {
            double mass = entity1.mass + entity2.mass;
            double r = Math.sqrt(entity1.sunRadius * entity1.sunRadius + entity2.sunRadius * entity2.sunRadius);
            double cX = (entity1.centerX * entity1.mass + entity2.centerX * entity2.mass) / mass;
            double cY = (entity1.centerY * entity1.mass + entity2.centerY * entity2.mass) / mass;
            double vX = (entity1.velX * entity1.mass + entity2.velX * entity2.mass) / mass;
            double vY = (entity1.velY * entity1.mass + entity2.velY * entity2.mass) / mass;
            Color col1 = entity1.sunColor;
            Color col2 = entity2.sunColor;
            int red = (col1.getRed() + col2.getRed()) / 2;
            int green = (col1.getGreen() + col2.getGreen()) / 2;
            int blue = (col1.getBlue() + col2.getBlue()) / 2;
            Color color = new Color(red, green, blue);
            SolarSystem newEntity = new SolarSystem(r, cX, cY, vX, vY, mass, color);
            GalaxyFrame.deleteEntity(entity1);
            GalaxyFrame.deleteEntity(entity2);
            GalaxyFrame.addEntity(newEntity);
        }
    }
}
