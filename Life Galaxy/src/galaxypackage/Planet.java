/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

/**
 *
 * @author Pablo
 */
import java.awt.Color;

public class Planet {

    SolarSystem parent;
    double centerX;
    double centerY;
    double radius;
    double orbitalRadius;
    double theta;
    double angularVelocity;
    double mass;
    Color color;

    public Planet(SolarSystem _parent, double _radius, double _orbitalRadius, double _theta0, double _angularVelocity, Color _color, double _mass) {
        parent = _parent;
        radius = _radius;
        orbitalRadius = _orbitalRadius;
        theta = _theta0;
        angularVelocity = _angularVelocity;
        centerX = parent.centerX + orbitalRadius * Math.cos(theta);
        centerY = parent.centerY + orbitalRadius * Math.sin(theta);
        color = _color;
        mass = _mass;
    }

    public void updateCoordinates(double elapsedTime) {
        theta += elapsedTime * angularVelocity;
        centerX = parent.centerX + orbitalRadius * Math.cos(theta);
        centerY = parent.centerY + orbitalRadius * Math.sin(theta);
    }
}
