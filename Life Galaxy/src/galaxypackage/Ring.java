/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;
import java.awt.Color;

/**
 *
 * @author Pablo
 */
public class Ring {    
    
    double innerRadius;
    double outerRadius;
    double mass;
    double mass0;
    Color color;
    SolarSystem owner;
    
    public Ring(double _innerRadius, double _outerRadius, Color _color, double _mass, SolarSystem entity) {
        innerRadius = _innerRadius;
        outerRadius = _outerRadius;
        color = _color;
        mass  = _mass;
        mass0 = _mass;
        owner = entity;
        
    }
}
