/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

import java.awt.Graphics2D;

/**
 *
 * @author Pablo
 */
public class SunWithPlanetsStatePainter1 extends StatePainter {

    @Override
    public void paint(Graphics2D g2d, SolarSystem entity) {
        paintCircle1(g2d, entity.centerX, entity.centerY, entity.sunRadius, entity.sunColor);
        for (Planet planet : entity.planets) {
            paintCircle1(g2d, planet.centerX, planet.centerY, planet.radius, planet.color);
        }
    }
}
