/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author Pablo
 */
public abstract class StatePainter {

    public static final CloudStatePainter1 cloudPainter1 = new CloudStatePainter1();
    public static final SunWithRingsStatePainter1 swrPainter1 = new SunWithRingsStatePainter1();
    public static final SunWithPlanetsStatePainter1 swpPainter1 = new SunWithPlanetsStatePainter1();
    public static final CloudStatePainter2 cloudPainter2 = new CloudStatePainter2();
    public static final SunWithRingsStatePainter2 swrPainter2 = new SunWithRingsStatePainter2();
    public static final SunWithPlanetsStatePainter2 swpPainter2 = new SunWithPlanetsStatePainter2();
    
    public static StatePainter cloudPainter;
    public static StatePainter swrPainter;
    public static StatePainter swpPainter;
    
    public static void SetPainters(int type) {
        if(type == 1) {
            cloudPainter = cloudPainter1;
            swrPainter = swrPainter1;
            swpPainter = swpPainter1;
        } else {
            cloudPainter = cloudPainter2;
            swrPainter = swrPainter2;
            swpPainter = swpPainter2;
        }
    }
    
    public void paintCircle1(Graphics2D g2d, double centerX, double centerY, double radius, Color color) {
        if (isThereIntersection(centerX - radius, centerX + radius, centerY - radius, centerY + radius,
                GalaxyFrame.bottomLeftX, GalaxyFrame.bottomLeftX + GalaxyCanvas.gridWidth,
                GalaxyFrame.bottomLeftY, GalaxyFrame.bottomLeftY + GalaxyCanvas.gridHeight));
        {
            int i, j;
            int xstart = (int) (centerX - radius - 1);
            int xend = (int) (centerX + radius + 1);
            int ystart = (int) (centerY - radius - 1);
            int yend = (int) (centerY + radius + 1);
            g2d.setColor(color);

            for (int x = xstart; x <= xend; x++) {
                for (int y = ystart; y <= yend; y++) {
                    if (isInsideCircle(x + 0.5, y + 0.5, centerX, centerY, radius)
                            && isInsideRectangle(x, y, GalaxyFrame.bottomLeftX, GalaxyFrame.bottomLeftY,
                            GalaxyCanvas.gridWidth, GalaxyCanvas.gridHeight)) {
                        i = x - GalaxyFrame.bottomLeftX;
                        j = y - GalaxyFrame.bottomLeftY;
                        g2d.fillRect((int) (i * GalaxyCanvas.cellWidth + 0.5),
                                (int) ((GalaxyCanvas.gridHeight - 1 - j) * GalaxyCanvas.cellHeight + 0.5),
                                (int) (GalaxyCanvas.cellWidth + 0.5),
                                (int) (GalaxyCanvas.cellHeight + 0.5));
                    }
                }
            }
        }
    }
    
    public void paintCircle2(Graphics2D g2d, double centerX, double centerY, double radius, Color color) {
        if (isThereIntersection(centerX - radius, centerX + radius, centerY - radius, centerY + radius,
                GalaxyFrame.bottomLeftX, GalaxyFrame.bottomLeftX + GalaxyCanvas.gridWidth,
                GalaxyFrame.bottomLeftY, GalaxyFrame.bottomLeftY + GalaxyCanvas.gridHeight));
        {
            g2d.setColor(color);
            double x = (centerX-GalaxyFrame.bottomLeftX-radius)*GalaxyCanvas.cellWidth;
            double y = (GalaxyCanvas.gridHeight - centerY + GalaxyFrame.bottomLeftY - radius)*GalaxyCanvas.cellHeight;
            double w = 2*radius*GalaxyCanvas.cellWidth;
            double h = 2*radius*GalaxyCanvas.cellHeight;            
            g2d.fill(new Ellipse2D.Double(x,y,w,h));
        }
    }

    protected boolean isThereIntersection(double lx1, double rx1, double by1, double uy1, double lx2, double rx2, double by2, double uy2) {
        return rx1 > lx2 && lx1 < rx2 && uy1 > by2 && by1 < uy2;
    }

    protected boolean isInsideRectangle(int x, int y, int refX, int refY, int width, int height) {
        return refX <= x && x < refX + width && refY <= y && y < refY + height;
    }

    protected boolean isInsideCircle(double x, double y, double Cx, double Cy, double R) {
        double dx = Cx - x;
        double dy = Cy - y;
        return dx * dx + dy * dy <= R * R;
    }

    public abstract void paint(Graphics2D g2d, SolarSystem entity);
}
