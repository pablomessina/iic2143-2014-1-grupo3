/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

/**
 *
 * @author Pablo
 */
public class SunWithPlanetsStateCollisionManager extends CollisionsManager {

    @Override
    protected void checkCollisionAgainstCloud(SolarSystem entity1, SolarSystem entity2) {
        CollisionsManager.cloudCollisionManager.checkCollisionAgainstSunWithPlanets(entity2, entity1);
    }

    @Override
    protected void checkCollisionAgainstSunWithRings(SolarSystem entity1, SolarSystem entity2) {
        CollisionsManager.swrCollisionManager.checkCollisionAgainstSunWithPlanets(entity2, entity1);
    }

    @Override
    protected void checkCollisionAgainstSunWithPlanets(SolarSystem entity1, SolarSystem entity2) {
        if (entity1.planets.size() > 0) {
            if (entity2.planets.size() > 0) {

                Planet p1 = entity1.planets.get(entity1.planets.size() - 1);
                Planet p2 = entity2.planets.get(entity2.planets.size() - 1);
                double lx1 = entity1.centerX - p1.orbitalRadius - p1.radius;
                double rx1 = entity1.centerX + p1.orbitalRadius + p1.radius;
                double by1 = entity1.centerY - p1.orbitalRadius - p1.radius;
                double uy1 = entity1.centerY + p1.orbitalRadius + p1.radius;
                double lx2 = entity2.centerX - p2.orbitalRadius - p2.radius;
                double rx2 = entity2.centerX + p2.orbitalRadius + p2.radius;
                double by2 = entity2.centerY - p2.orbitalRadius - p2.radius;
                double uy2 = entity2.centerY + p2.orbitalRadius + p2.radius;

                if (!this.isThereIntersectionRectangles(lx1, rx1, by1, uy1, lx2, rx2, by2, uy2)) {
                    return;
                }

                for (int i = entity1.planets.size() - 1; i >= 0; --i) {
                    p1 = entity1.planets.get(i);
                    for (int j = entity2.planets.size() - 1; j >= 0; --j) {
                        p2 = entity2.planets.get(j);
                        if (this.isThereIntersection(p1, p2)) {
                            entity1.planets.remove(i);
                            entity2.planets.remove(j);
                            break;
                        }
                    }
                }
            }
        }
        for (int i = entity1.planets.size() - 1; i >= 0; --i) {
            Planet p = entity1.planets.get(i);
            if (this.isThereIntersection(p, entity2)) {
                entity1.planets.remove(i);
                entity2.sunRadius *= Math.sqrt(1 + p.mass / entity2.mass);
                entity2.mass += p.mass;
            }
        }
        for (int i = entity2.planets.size() - 1; i >= 0; --i) {
            Planet p = entity2.planets.get(i);
            if (this.isThereIntersection(p, entity1)) {
                entity2.planets.remove(i);
                entity1.sunRadius *= Math.sqrt(1 + p.mass / entity2.mass);
                entity1.mass += p.mass;
            }
        }
        CollisionsManager.cloudCollisionManager.checkCollisionAgainstCloud(entity1, entity2);
    }
}
