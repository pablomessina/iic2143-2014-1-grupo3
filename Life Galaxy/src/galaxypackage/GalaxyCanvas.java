/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Pablo
 */
public class GalaxyCanvas extends JPanel {
    
    static double cellWidth;
    static double cellHeight;
    static int gridWidth;
    static int gridHeight;
    static double threshold = 1.2;

    public GalaxyCanvas() {
    }
    
    public void setResolution(int _gridWidth, int _gridHeight) {
        
        Dimension size = getSize();
        gridWidth = _gridWidth;
        gridHeight = _gridHeight;
        cellWidth = ((double)size.width)/gridWidth;        
        cellHeight = ((double)size.height)/ gridHeight;
        
        if(cellWidth <= threshold || cellHeight <= threshold) {
            StatePainter.SetPainters(2);
        } else {
            StatePainter.SetPainters(1);
        }
    }
    
   

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        Dimension size = getSize();
        g2d.setColor(Color.black);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.fillRect(0, 0, size.width, size.height);

        if (!GalaxyFrame.simulationStarted) {
            return;
        }
        
        synchronized(GalaxyFrame.class) {
            for (SolarSystem entity : GalaxyFrame.entities.values()) {
                entity.painter.paint(g2d, entity);
            }
        }
    }
}
