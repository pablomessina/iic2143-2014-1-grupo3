/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

/**
 *
 * @author Pablo
 */
public abstract class CollisionsManager {

    public static class idPair {

        long id1;
        long id2;

        private idPair(long id1, long id2) {
            this.id1 = id1;
            this.id2 = id2;
        }
    }

    public static class dynamicIdPairArray {

        idPair[] idpairs;
        int size;

        public dynamicIdPairArray(int n) {
            size = -1;
            idpairs = new idPair[n];
        }

        public void add(idPair pair) {
            ++size;
            idpairs[size] = pair;
            if (size + 1 == idpairs.length) {
                idPair[] aux = new idPair[idpairs.length * 2];
                for (int i = 0; i < idpairs.length; ++i) {
                    aux[i] = idpairs[i];
                }
                idpairs = aux;
            }
        }

        public idPair getAt(int i) {
            return idpairs[i];
        }

        public void deleteAt(int i) {
            idpairs[i] = idpairs[size];
            --size;
        }
    }
    static final CloudStateCollisionManager cloudCollisionManager = new CloudStateCollisionManager();
    static final SunWithRingsStateCollisionManager swrCollisionManager = new SunWithRingsStateCollisionManager();
    static final SunWithPlanetsStateCollisionManager swpCollisionManager = new SunWithPlanetsStateCollisionManager();
    static final Object collisionslock = new Object();
    static dynamicIdPairArray idpairs;

    public static void initIdpairs(int n) {
        idpairs = new dynamicIdPairArray(n);
    }

    public static void checkCollisions() {
        SolarSystem entity1, entity2;
        long id1, id2;
        idPair idpair;
        for (int i = 0; i < idpairs.size; ++i) {
            idpair = idpairs.getAt(i);
            id1 = idpair.id1;
            id2 = idpair.id2;
            if (GalaxyFrame.ids.contains(id1) && GalaxyFrame.ids.contains(id2)) {
                entity1 = GalaxyFrame.entities.get(id1);
                entity2 = GalaxyFrame.entities.get(id2);
                checkCollisions(entity1, entity2);
            } else {
                idpairs.deleteAt(i);
                --i;
            }
        }
    }

    private static void checkCollisions(SolarSystem entity1, SolarSystem entity2) {
        switch (entity2.state) {
            case Cloud:
                entity1.collisionManager.checkCollisionAgainstCloud(entity1, entity2);
                break;
            case Sun_with_rings:
                entity1.collisionManager.checkCollisionAgainstSunWithRings(entity1, entity2);
                break;
            case Sun_with_planets:
                entity1.collisionManager.checkCollisionAgainstSunWithPlanets(entity1, entity2);
                break;
        }
    }

    public static void updateIdPairs(long newId) {
        for (long id : GalaxyFrame.ids) {
            if (id != newId) {
                idpairs.add(new idPair(newId, id));
            }
        }
    }

    protected abstract void checkCollisionAgainstCloud(SolarSystem entity1, SolarSystem entity2);

    protected abstract void checkCollisionAgainstSunWithRings(SolarSystem entity1, SolarSystem entity2);

    protected abstract void checkCollisionAgainstSunWithPlanets(SolarSystem entity1, SolarSystem entity2);

    protected boolean isThereIntersectionRectangles(double lx1, double rx1, double by1, double uy1, double lx2, double rx2, double by2, double uy2) {
        return rx1 > lx2 && lx1 < rx2 && uy1 > by2 && by1 < uy2;
    }

    protected boolean isInsideRectangle(int x, int y, int refX, int refY, int width, int height) {
        return refX <= x && x < refX + width && refY <= y && y < refY + height;
    }

    protected boolean isInsideCircle(double x, double y, double Cx, double Cy, double R) {
        double dx = Cx - x;
        double dy = Cy - y;
        return dx * dx + dy * dy <= R * R;
    }

    protected boolean isThereIntersectionCircles(double cx1, double cy1, double cx2, double cy2, double r1, double r2) {
        double dx = cx1 - cx2;
        double dy = cy1 - cy2;
        return Math.sqrt(dx * dx + dy * dy) <= r1 + r2;
    }

    protected boolean isThereIntersection(Planet planet, Ring ring) {
        double dx = planet.centerX - ring.owner.centerX;
        double dy = planet.centerY - ring.owner.centerY;
        double d = Math.sqrt(dx * dx + dy * dy);
        return d - planet.radius < ring.outerRadius && d + planet.radius > ring.innerRadius;
    }

    protected boolean isThereIntersection(Planet planet, SolarSystem system) {
        double dx = planet.centerX - system.centerX;
        double dy = planet.centerY - system.centerY;
        return Math.sqrt(dx * dx + dy * dy) < planet.radius + system.sunRadius;
    }

    protected boolean isThereIntersection(Ring ring, SolarSystem system) {
        double dx = system.centerX - ring.owner.centerX;
        double dy = system.centerY - ring.owner.centerY;
        return Math.sqrt(dx * dx + dy * dy) <= ring.outerRadius + system.sunRadius;
    }

    protected boolean isThereIntersection(Planet p1, Planet p2) {
        double dx = p1.centerX - p2.centerX;
        double dy = p1.centerY - p2.centerY;
        return Math.sqrt(dx * dx + dy * dy) < p1.radius + p2.radius;
    }
}
