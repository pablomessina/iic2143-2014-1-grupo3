/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random; 

enum State {

    Cloud, Sun_with_rings, Sun_with_planets
};

public class SolarSystem implements Runnable {

    static final Random random = new Random();
    static final double G = 5;
    double centerX;
    double centerY;
    double sunRadius;
    double velX, velY;
    double accX, accY;
    double mass;
    Color sunColor;
    long id;
    State state;
    long age = 0;
    StatePainter painter;
    CollisionsManager collisionManager;
    ArrayList<Planet> planets;
    ArrayList<Ring> rings;
    static long sunWithRings_Age = 200;
    static long sunWithPlanets_Age = 400;
    static long idcount = 0;
    boolean alive = true;
    Thread wrapperThread;

    public SolarSystem(double _sunRadius, double _centerX, double _centerY, double _velX, double _velY, double _mass, Color _sunColor) {
        centerX = _centerX;
        centerY = _centerY;
        velX = _velX;
        velY = _velY;
        mass = _mass;
        sunRadius = _sunRadius;
        sunColor = _sunColor;
        state = State.Cloud;
        painter = StatePainter.cloudPainter;
        collisionManager = CollisionsManager.cloudCollisionManager;
        id = idcount++;
        System.out.println("entidad " + id + " creada");
    }

    @Override
    public void run() {
        double r, r3;
        double dx, dy;
        try {
            while (alive) {
                synchronized (GalaxyFrame.timerlock) {
                    GalaxyFrame.timerlock.wait();
                }
                for (SolarSystem system: GalaxyFrame.entities.values()) {
                    if (system == this) {
                        continue;
                    }
                    dx = system.centerX - this.centerX;
                    dy = system.centerY - this.centerY;
                    r = Math.sqrt(dx * dx + dy * dy);
                    r3 = r * r * r;
                    accX += G * system.mass * dx / r3;
                    accY += G * system.mass * dy / r3;
                }

                barrier();

                centerX += velX * GalaxyFrame.intervalTime;
                centerY += velY * GalaxyFrame.intervalTime;
                velX += accX * GalaxyFrame.intervalTime;
                velY += accY * GalaxyFrame.intervalTime;

                age++;

                switch (state) {
                    case Cloud:
                        if (age == sunWithRings_Age) {
                            transitionToSunWithRingsState();
                        }
                        break;
                    case Sun_with_rings:
                        if (age == sunWithPlanets_Age) {
                            transitionToSunWithPlanetsState();
                        }
                        break;
                    case Sun_with_planets:
                        for (Planet planet : planets) {
                            planet.updateCoordinates(GalaxyFrame.intervalTime);
                        }
                        break;
                }                
                collisionsBarrier();
            }
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    void barrier() throws InterruptedException {
        synchronized (GalaxyFrame.entitieslock) {
            GalaxyFrame.count++;
            if (GalaxyFrame.count == GalaxyFrame.entities.size()) {
                GalaxyFrame.count = 0;
                GalaxyFrame.entitieslock.notifyAll();
            } else {
                GalaxyFrame.entitieslock.wait();
            }
        }
    }

    void collisionsBarrier() {
        synchronized (GalaxyFrame.entitieslock) {
            GalaxyFrame.count++;
            if (GalaxyFrame.count == GalaxyFrame.entities.size()) {
                GalaxyFrame.count = 0;
                synchronized(GalaxyFrame.class) {
                    CollisionsManager.checkCollisions();
                    GalaxyFrame.addDragAndDropEntities();
                    GalaxyFrame.startNewEntities();
                }                
                GalaxyFrame.entitieslock.notifyAll();
            } else {
                try {
                    GalaxyFrame.entitieslock.wait();
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    void transitionToSunWithRingsState() {
        state = State.Sun_with_rings;
        rings = new ArrayList<>();
        double previousRadius = sunRadius;
        sunRadius *= 0.1 + random.nextDouble() * 0.3;
        double coef = 0.8;
        double prevMass = mass;
        mass *= coef;
        int ringsNumber = 1 + random.nextInt(5);
        double remainderRadius = previousRadius - sunRadius;
        double delta = remainderRadius / (ringsNumber + 1);
        double width = 0.2 * delta;
        double ringMass = prevMass * 0.2 / ringsNumber;
        for (int i = 1; i <= ringsNumber; i++) {
            rings.add(new Ring(sunRadius + i * delta - width, sunRadius + i * delta + width,
                    new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256)),
                    ringMass, this));
        }
        painter = StatePainter.swrPainter;
        collisionManager = CollisionsManager.swrCollisionManager;
    }

    void transitionToSunWithPlanetsState() {
        state = State.Sun_with_planets;
        planets = new ArrayList<>();
        for (Ring ring : rings) {
            double radius = Math.sqrt((ring.outerRadius * ring.outerRadius - ring.innerRadius * ring.innerRadius) * 0.05);
            double angle = 2 * Math.PI * random.nextDouble();
            double orbitalRadius = (ring.outerRadius + ring.innerRadius) / 2;
            double angular_velocity = 100 * (1 + random.nextDouble()) / orbitalRadius;
            planets.add(new Planet(this, radius, orbitalRadius, angle, angular_velocity, ring.color, ring.mass));
        }
        painter = StatePainter.swpPainter;
        collisionManager = CollisionsManager.swpCollisionManager;
    }
}
