/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package galaxypackage;

import java.awt.Color;

/**
 *
 * @author Pablo
 */
public class SunWithRingsStateCollisionManager extends CollisionsManager {

    @Override
    protected void checkCollisionAgainstCloud(SolarSystem entity1, SolarSystem entity2) {
        entity2.collisionManager.checkCollisionAgainstSunWithRings(entity2, entity1);
    }

    @Override
    protected void checkCollisionAgainstSunWithRings(SolarSystem entity1, SolarSystem entity2) {
        if (entity1.rings.size() > 0) {
            Ring ring1 = entity1.rings.get(entity1.rings.size() - 1);
            if (entity2.rings.size() > 0) {
                Ring ring2 = entity2.rings.get(entity2.rings.size() - 1);
                if (isThereIntersectionCircles(entity1.centerX, entity1.centerY, entity2.centerX, entity2.centerY, ring1.outerRadius, ring2.outerRadius)) {
                    if (ring1.mass < ring2.mass) {
                        Ring aux = ring1;
                        ring1 = ring2;
                        ring2 = aux;
                        entity1 = entity2;
                    }
                    double totalmass = ring1.mass + ring2.mass;
                    ring1.color = new Color((int) ((ring1.color.getRed() * ring1.mass + ring2.color.getRed() * ring2.mass) / totalmass),
                            (int) ((ring1.color.getGreen() * ring1.mass + ring2.color.getGreen() * ring2.mass) / totalmass),
                            (int) ((ring1.color.getBlue() * ring1.mass + ring2.color.getBlue() * ring2.mass) / totalmass));
                    ring1.mass = totalmass;
                    entity1.rings.remove(entity1.rings.size() - 1);
                }
            } else {
                CollisionsManager.cloudCollisionManager.checkCollisionAgainstSunWithRings(entity2, entity1);
            }
        } else {
            if (entity2.rings.size() > 0) {
                CollisionsManager.cloudCollisionManager.checkCollisionAgainstSunWithRings(entity1, entity2);
            } else {
                CollisionsManager.cloudCollisionManager.checkCollisionAgainstCloud(entity1, entity2);
            }
        }
    }

    @Override
    protected void checkCollisionAgainstSunWithPlanets(SolarSystem entity1, SolarSystem entity2) {
        if (entity1.rings.size() > 0) {
            if (entity2.planets.size() > 0) {

                Planet planet = entity2.planets.get(entity2.planets.size() - 1);
                Ring ring = entity1.rings.get(entity1.rings.size() - 1);

                double lx1 = entity2.centerX - planet.orbitalRadius - planet.radius;
                double rx1 = entity2.centerX + planet.orbitalRadius + planet.radius;
                double by1 = entity2.centerY - planet.orbitalRadius - planet.radius;
                double uy1 = entity2.centerY + planet.orbitalRadius + planet.radius;
                double lx2 = entity1.centerX - ring.outerRadius;
                double rx2 = entity1.centerX + ring.outerRadius;
                double by2 = entity1.centerY - ring.outerRadius;
                double uy2 = entity1.centerY + ring.outerRadius;

                if (!this.isThereIntersectionRectangles(lx1, rx1, by1, uy1, lx2, rx2, by2, uy2)) {
                    return;
                }

                for (int i = entity2.planets.size() - 1; i >= 0; --i) {
                    planet = entity2.planets.get(i);
                    int j = entity1.rings.size();
                    while (--j >= 0) {
                        ring = entity1.rings.get(j);
                        if (this.isThereIntersection(planet, ring)) {
                            double dm = ring.mass * (0.44 * planet.radius * planet.radius) / (ring.outerRadius * ring.outerRadius - ring.innerRadius * ring.innerRadius);
                            planet.radius *= Math.sqrt(1 + dm / planet.mass);
                            ring.mass -= dm;
                            double newmass = planet.mass + dm;
                            planet.color = new Color((int) ((planet.color.getRed() * planet.mass + ring.color.getRed() * dm) / newmass),
                                    (int) ((planet.color.getGreen() * planet.mass + ring.color.getGreen() * dm) / newmass),
                                    (int) ((planet.color.getBlue() * planet.mass + ring.color.getBlue() * dm) / newmass));
                            planet.mass = newmass;
                            if (ring.mass < 0.01) {
                                entity1.rings.remove(j);
                            } else {
                                ring.color = new Color(ring.color.getRed(), ring.color.getGreen(), ring.color.getBlue(), (int) (ring.mass / ring.mass0) * 255);
                            }
                        }
                    }
                }
            }
        }
        for (int i = entity2.planets.size() - 1; i >= 0; --i) {
            Planet planet = entity2.planets.get(i);
            if (this.isThereIntersection(planet, entity1)) {
                entity2.planets.remove(i);
                entity1.sunRadius *= Math.sqrt(1 + planet.mass / entity1.mass);
                entity1.mass += planet.mass;
            }
        }
        for (int i = entity1.rings.size() - 1; i >= 0; --i) {
            Ring ring = entity1.rings.get(i);
            if (this.isThereIntersection(ring, entity2)) {
                entity1.rings.remove(i);
                entity2.sunRadius *= Math.sqrt(1 + ring.mass / entity2.mass);
                entity2.mass += ring.mass;
            }
        }
        CollisionsManager.cloudCollisionManager.checkCollisionAgainstCloud(entity1, entity2);
    }
}
