/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Networking;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 *
 * @author Enzo
 */
//Esta es la clase servidor, fundamental para el proyecto ya que siempre se estará recibiendo entidades

public class Server extends Thread
{
    
    //Va con una clase Main para poder probar la conexión
      public static void main(String [] args)
   {
//    //El thread anteriormente mencionado
      try
      {
         Thread t = new Server(80);
         t.start();
      }catch(IOException e)
      {
         e.printStackTrace();
      }
   }
    
    
//Se crea un ServerSocket
    private ServerSocket serverSocket;
   //Por ahora se inicia con valores localhost, luego se pedirán datos para crear una conexión real.
   public Server(int port) throws IOException
   {
      serverSocket = new ServerSocket(port);
      serverSocket.setSoTimeout(10000000);
      
      //Se inicia con un gran tiempo de espera.
   }

   public void run()
   {
       //Estees el método más importante ya que es el que escucha las conexiones, como estas son muchas se crea un Thread luego para
       //seguir escuchando
      while(true)
      {
          
         try
         {
            System.out.println("Esperando conexiones " +
            serverSocket.getLocalPort() + "...");
            Socket server = serverSocket.accept();
            System.out.println("Se estableció conexión con "
                  + server.getRemoteSocketAddress());
            DataInputStream in =
                  new DataInputStream(server.getInputStream());
            System.out.println(in.readUTF());
            DataOutputStream out =
                 new DataOutputStream(server.getOutputStream());
            out.writeUTF("La conexión se estableció con exito "
              + server.getLocalSocketAddress() + "\n Cacacaccaca");
            server.close();
         }catch(SocketTimeoutException s)
         {
            System.out.println("Se agotó el tiempo de espera, error número 69...!");
            break;
         }catch(IOException e)
         {
            e.printStackTrace();
            break;
         }
      }
   }
 
}

