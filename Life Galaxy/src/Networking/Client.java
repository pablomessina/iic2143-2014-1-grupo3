/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Networking;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 *
 * @author Enzo
 */
public class Client {
    
    //Se crea la clase cliente para poder conectarse con otros proyectos para luego enviar serializado el objeto que es la
    //entidad de cada Thread
   public static void main(String [] args)
   {
      //Para esta entrega se usará solo localhost y se creará un chat para verificar el envío de datos.
      String serverName = "127.0.0.1";
      int port = 80;
      //Además se usará el puerto 80
      try
      {
         //Esto es para ir imprimiendo lo necesario
         System.out.println("Intentando conexión " + serverName
                             + " en el ppuerto" + port);
         
         
         //Se inicia la conexión con el servidor, el server name es localhost
         Socket client = new Socket(serverName, port);
         //Se intenta enchufar
         System.out.println("Se ha conectado a "
                      + client.getRemoteSocketAddress());
         OutputStream outToServer = client.getOutputStream();
         DataOutputStream out =
                       new DataOutputStream(outToServer);
        
         
         
         
         out.writeUTF("Conectado al servidor "
                      + client.getLocalSocketAddress());
         InputStream inFromServer = client.getInputStream();
         DataInputStream in =
                        new DataInputStream(inFromServer);
         System.out.println("Mensaje del servidor " + in.readUTF());
         
         client.close();
      }catch(IOException e)
      {
         e.printStackTrace();
      }
   }
}
