# Entrega 6 #

### Versión a corregir: ###

La branch E6_Eclipse tiene el proyecto actual en Eclipse y los documentos requeridos (metrics, jprofiler y ant)

La branch Serializador Parcial tiene el proyecto en netbeans (y también los documentos requeridos) 

[actualización (03-07-2014): se agregó un nuevo commit a la branch serializador parcial con el código de la migración de threads actualizado (usando protocolo del foro) ya que hubo errores de compilación cuando estos cambios se agregaron en la branch de Entrega Final]

### Manual de uso: ###

1. El programa corre en Eclipse, pero se recomienda probarlo en Netbeans ya que se producen pequeñas variaciones en la interfaz.

2. En la pantalla de inicio dar click a "Comenzar"

3. Para iniciar la simulación dar click a "Run Simulation" en la esquina inferior derecha,
aparecerán en la pantalla los hoyos para transportar entidades a otras galaxias.

4. Para sumar nuevas entidades a la simulación seleccionar un tipo: "Barnard", "Cefeo" u 
"Orion". Y luego arrastar desde el símbolo galaxia (verde) hasta la posición en que quiere ubicarse.

### Especificaciones de tareas: ###

Camila Neira: Visibilidad descripciones, JProfiler, análisis y generación de documento Metrics
Pablo Messina: JUnit
Rodolfo Vargas: Cambios a la interfaz gráfica, Ant, Diagrama UML
Enzo Zerega: Cambios a la interfaz gráfica, Adaptación IDE (Netbeans a Eclipse)
-----------------------------------------------------------------------------------------------------
# Entrega 4 #

Commit final a evaluar: 
4dab3c7 : Mejoras visuales, cambiado el comportamiento de botones Run
 y direccionales, agregada opción de visualizar estado de entidades 
con click derecho
6395206: Contiene buena parte de la entrega, sin embargo no está en el master porque aún produce muchos problemas al enviar entidades entre dos instancias del juego. Sin embargo la serialización y el envío del objeto como archivo se logra.

### Especificaciones de tareas: ###

serialización y deserialización de entidades, Cominicacion Cliente-Servidor y envío de archivos: Enzo Zerega y Camila Neira

Adaptación de IDE (Netbeans a Eclipse): Enzo Zerega
 
Arreglos al sistema de sincronización y colisiones: Pablo Messina 
(compatible con drag and drop y con eliminar entidades por serialización)
 
Arreglos a interfaz y mejoras visuales (comportamiento de botones, finalización de drag and drop y adición de click derecho para ver info de entidades), diagrama UML a partir de código: Rodolfo Vargas
 
Adaptación del algoritmo de dibujado según resolución: Pablo Messina